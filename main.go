package main

import (
	"net/http"
	"fmt"
	"borgdir.media/app/handler"
)

var port string = ":80"

func main() {
	
    /* Handler */
    index 			:= handler.Handler_Index{}
    login 			:= handler.Handler_Login{}
    logout			:= handler.Handler_Logout{}
    admin 			:= handler.Handler_Admin{}
    equipment 		:= handler.Handler_Equipment{}
    my_equipment	:= handler.Handler_MeinEquipment{}
    profil			:= handler.Handler_Profil{}
    deleteProfil	:= handler.Handler_DeleteProfil{}
    profilSperren	:= handler.Handler_ProfilSperren{}
    register		:= handler.Handler_Register{}
    warenkorb		:= handler.Handler_Warenkorb{}
    inWarenkorb		:= handler.Handler_InWarenkorb{}
    ausWarenkorb	:= handler.Handler_AusWarenkorb{}
    ausVormerkungen	:= handler.Handler_AusVormerkungen{}
    
    ausleihen		:= handler.Handler_Ausleihen{}
    verlaengern		:= handler.Handler_Verlaengern{}
    profilbild		:= handler.Handler_UploadProfilbild{}
    equipmentbild	:= handler.Handler_UploadEquipmentbild{}
    clientbild		:= handler.Handler_UploadClientbild{}
    
    /* Handler des Adminbereichs */
    adminEquipment	:= handler.Handler_AdminEquipment{}
    deleteItem		:= handler.Handler_DeleteItem{}
    add				:= handler.Handler_Add{}
    clients			:= handler.Handler_Clients{}
    editClient		:= handler.Handler_EditClient{}
    editItem		:= handler.Handler_EditItem{}
    
    
    /* Fileserver für CSS, Scripts und Bilder */
    css := http.FileServer(http.Dir("../src/borgdir.media/static/css"))
	http.Handle("/css/", http.StripPrefix("/css/", css))
    
    scripts := http.FileServer(http.Dir("../src/borgdir.media/static/scripts"))
	http.Handle("/scripts/", http.StripPrefix("/scripts/", scripts))
	
	plugins := http.FileServer(http.Dir("../src/borgdir.media/static/plugins"))
	http.Handle("/plugins/", http.StripPrefix("/plugins/", plugins))
	
	images := http.FileServer(http.Dir("../src/borgdir.media/static/images"))
	http.Handle("/images/", http.StripPrefix("/images/", images))
    
    
    /* Server */
    server := http.Server{
		Addr: port,
    }


	/* Registriere Handler */
    http.Handle("/", &index)
    http.Handle("/login/", &login)
    http.Handle("/logout/", &logout)
    http.Handle("/admin/", &admin)
    http.Handle("/equipment/", &equipment)
    http.Handle("/my-equipment/", &my_equipment)
    http.Handle("/profil/", &profil)
    http.Handle("/deleteProfil/", &deleteProfil)
    http.Handle("/register/", &register)
    http.Handle("/cart/", &warenkorb)
    http.Handle("/InWarenkorb/", &inWarenkorb)
    http.Handle("/AusWarenkorb/", &ausWarenkorb)
    http.Handle("/AusVormerkungen/", &ausVormerkungen)
    
    http.Handle("/ausleihen/", &ausleihen)
    http.Handle("/verlaengern/", &verlaengern)
    http.Handle("/upload-profilbild/", &profilbild)
    http.Handle("/upload-equipmentbild/", &equipmentbild)
    http.Handle("/upload-profilbild-client/", &clientbild)
    
    http.Handle("/admin/equipment/", &adminEquipment)
    http.Handle("/admin/edit-item/", &editItem)
    http.Handle("/admin/delete-item/", &deleteItem)
    http.Handle("/admin/add/", &add)
    http.Handle("/admin/clients/", &clients)
    http.Handle("/admin/profil-sperren/", &profilSperren)
    http.Handle("/admin/edit-client/", &editClient)
    
    
    fmt.Println("Server Gestartet unter Port " + port)
    
    
    /* Starte Server */
    server.ListenAndServe()   
}

