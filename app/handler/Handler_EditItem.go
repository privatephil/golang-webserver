package handler

import (
	"borgdir.media/app/model"
	"borgdir.media/config"
	"fmt"
	"html/template"
	"net/http"
	"strconv"
)

type Handler_EditItem struct{}

type EditItemSpezial struct {
	Item      model.Item
	Equipment model.Equipment
}

func (h *Handler_EditItem) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Itemverwaltung Handler serving...")

	if r.Method == "GET" {

		/* Session */
		var id int
		w, r, id = handleSession(w, r)

		if id != 0 {

			acc, _ := model.GetById_Account(id)

			if acc.Admin == 1 {

				q := r.URL.Query()

				fmt.Println("IdItem: " + q.Get("idItem"))

				idItem, _ := strconv.Atoi(q.Get("idItem"))
				item, _ := model.GetById_Item(idItem)
				equipment, _ := model.GetById_Equipment(item.IdEquipment)

				itemSpezial := EditItemSpezial{
					Item:      item,
					Equipment: equipment,
				}

				fmt.Println("Item Spezial erzeugt")
				fmt.Println(itemSpezial)

				w.Header().Set("Content-Type", "text/html")
				w.WriteHeader(http.StatusOK)

				filePath := config.PfadAdmin + "/edit-equipment.html"
				head := config.PfadTemplate + "/head.html"
				header := config.PfadTemplate + "/headerUser.html"
				footer := config.PfadTemplate + "/footer.html"
				content := config.PfadTemplate + "/contentEditEquipment.html"

				tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)
				
				tmpl.ExecuteTemplate(w, "header", acc)
				tmpl.ExecuteTemplate(w, "editEquipment", itemSpezial)
			} else {
				w.Header().Set("Content-Type", "text/html")
				w.WriteHeader(http.StatusOK)

				filePath := config.PfadStatic + "/index.html"
				head := config.PfadTemplate + "/head.html"
				header := config.PfadTemplate + "/headerUser.html"
				footer := config.PfadTemplate + "/footer.html"
				content := config.PfadTemplate + "/contentIndex.html"

				tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)

				tmpl.ExecuteTemplate(w, "index", acc)
			}
		} else {
			w.Header().Set("Content-Type", "text/html")
			w.WriteHeader(http.StatusOK)

			filePath := config.PfadStatic + "/index.html"
			head := config.PfadTemplate + "/head.html"
			header := config.PfadTemplate + "/header.html"
			footer := config.PfadTemplate + "/footer.html"
			content := config.PfadTemplate + "/contentIndex.html"

			tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)

			tmpl.ExecuteTemplate(w, "index", "")
		}
	} else if r.Method == "POST" {

		fmt.Println("Edit Client POST")

		/* Session */
		var id int
		w, r, id = handleSession(w, r)

		if id != 0 {

			acc1, _ := model.GetById_Account(id)

			if acc1.Admin == 1 {

				r.ParseForm()

				fmt.Println(r.Form["bezeichnung"][0])
				fmt.Println(r.Form["inhalt"][0])
				fmt.Println(r.Form["beschreibung"][0])
				fmt.Println(r.Form["inventar-nummer"][0])

				idItem, _ := strconv.Atoi(r.Form["inventar-nummer"][0])

				item, _ := model.GetById_Item(idItem)
				equipment, _ := model.GetById_Equipment(item.IdEquipment)
				
				kategorieNeu := r.Form["kategorie"][0]
				var k int
				switch kategorieNeu {
					case "kameras":
						fmt.Println("Neue Kategorie ist 1")
						k = 1
					case "mikrofone":
						fmt.Println("Neue Kategorie ist 2")
						k = 2
					case "pa":
						fmt.Println("Neue Kategorie ist 3")
						k = 3
					case "zubehoer":
						fmt.Println("Neue Kategorie ist 4")
						k = 4
				}
				
				newEquip := model.Equipment{
					IdEquipment: 	equipment.IdEquipment,
					IdKategorie: 	k,
					Picture: 		equipment.Picture,
					Name:			r.Form["bezeichnung"][0],
					Beschreibung:	r.Form["beschreibung"][0],
					Inhalt:			r.Form["inhalt"][0],
				}
				
				fmt.Println("Equipment Id: " + strconv.Itoa(equipment.IdEquipment) )
				
				newEquip.AlterEquipment()
				
				lagerortNeu := r.Form["lagerort"][0]
				var l int
				switch lagerortNeu {
					case "schrank":
						l = 1
					case "dachlager":
						l = 2
					case "lager 1":
						l = 3
					case "lager 2":
						l = 4
					case "transporter":
						l = 5
				}
				
				items, _ := model.GetAll_Item()
				
				for _, i := range items {
					if i.IdEquipment == newEquip.IdEquipment {
						newItem := model.Item {
							IdItem:      i.IdItem,
							IdEquipment: i.IdEquipment,
							IdLagerort:  l,
							Entliehen:   i.Entliehen,
						}
						
						newItem.AlterItem()
					}
				}
				
				fmt.Println("Equipment geändert!")

				w.Header().Set("Content-Type", "text/html")
				w.WriteHeader(http.StatusOK)

				tmpl, _ := template.ParseFiles(config.PfadTemplate + "/feedback/itemGeaendert.html")

				tmpl.ExecuteTemplate(w, "itemGeaendert", nil)
				
			} else {
				w.Header().Set("Content-Type", "text/html")
				w.WriteHeader(http.StatusOK)

				tmpl, _ := template.ParseFiles(config.PfadTemplate + "/feedback/clientGeaendertPasswoerterFalsch.html")

				tmpl.ExecuteTemplate(w, "clientGeaendertPasswoerterFalsch", nil)
			}
		}
	}
}
