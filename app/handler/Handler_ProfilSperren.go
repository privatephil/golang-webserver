package handler

import (
	"borgdir.media/app/model"
	"borgdir.media/config"
	"fmt"
	"html/template"
	"net/http"
	"strconv"
)

type Handler_ProfilSperren struct{}

func (h *Handler_ProfilSperren) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	if r.Method == "GET" {
		
		fmt.Println("Account soll gesperrt werden")
		
		/* Session */
		var id int
		w, r, id = handleSession(w, r)

		if id != 0 {
			acc, _ := model.GetById_Account(id)

			if acc.Admin == 1 {
				
				fmt.Println("Admin hat Zugriffsrecht")
				
				q := r.URL.Query()

				idAcc, _ := strconv.Atoi( q.Get("idAccount") )
				
				fmt.Println("Id des Accounts ist: " + strconv.Itoa(idAcc) )
				
				accProfil, _ := model.GetById_Account( idAcc )

				newAcc := model.Account{
					IdAccount:    accProfil.IdAccount,
					Benutzername: accProfil.Benutzername,
					Passwort:     accProfil.Passwort,
					Email:        accProfil.Email,
					Admin:        accProfil.Admin,
					Gesperrt:     1,
					Aktiv_bis:    "0",
				}

				newAcc.AlterAccount()

				fmt.Println("Account gesperrt!")

				w.Header().Set("Content-Type", "text/html")
				w.WriteHeader(http.StatusOK)

				tmpl, _ := template.ParseFiles(config.PfadTemplate + "/feedback/accountGesperrt.html")

				tmpl.ExecuteTemplate(w, "accountGesperrt", nil)
			}
		}
	}
}
