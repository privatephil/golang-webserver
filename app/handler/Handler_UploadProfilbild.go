package handler

import (
	"borgdir.media/app/model"
	"borgdir.media/config"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strconv" // String to Int Converter
)

type Handler_UploadProfilbild struct{}

func (h *Handler_UploadProfilbild) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	fmt.Println("Upload Profilbild")

	/* Session */
	var id int
	w, r, id = handleSession(w, r)

	if id != 0 {
		fmt.Println("Nutzer erkannt")
		r.ParseMultipartForm(32)

		file, _, err := r.FormFile("picture")

		if err != nil {
			fmt.Println("Ein Fehler beim hochladen des Bildes ist aufgetreten!")
			return
		}
		defer file.Close()
		fmt.Println("defer file.Close()")
		fileBytes, err := ioutil.ReadAll(file)

		fmt.Println(fileBytes)

		if err != nil || len(fileBytes) == 0 {
			fmt.Println("Ein Fehler beim hochladen des Bildes ist aufgetreten!")
			return
		}

		fileName := strconv.Itoa(id) + ".jpg"
		newPath := filepath.Join(config.PfadStatic+"/images/profiles", fileName)

		newFile, _ := os.Create(newPath)
		defer newFile.Close()
		fmt.Println("defer newFile.Close()")
		_, err = newFile.Write(fileBytes)

		if err != nil {
			fmt.Println("Ein Fehler beim hochladen des Bildes ist aufgetreten!")
			return
		}

		account, _ := model.GetById_Account(id)
		account.Picture = 1

		err = account.AlterAccount()

		if err != nil {
			fmt.Println("Account konnte nicht abgeändert werden!")
			return
		}

		w.Header().Set("Content-Type", "text/html")
		w.WriteHeader(http.StatusOK)

		filePath := config.PfadHtml + "/profil.html"
		head := config.PfadTemplate + "/head.html"
		header := config.PfadTemplate + "/headerUser.html"
		footer := config.PfadTemplate + "/footer.html"
		content := config.PfadTemplate + "/contentProfil.html"

		tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)

		tmpl.ExecuteTemplate(w, "profil", account)
	}
}
