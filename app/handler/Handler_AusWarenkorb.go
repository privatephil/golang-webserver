package handler

import (
	"borgdir.media/app/model"
	"borgdir.media/config"
	"fmt"
	"html/template"
	"net/http"
	"strconv"
)

type Handler_AusWarenkorb struct{}

func (h *Handler_AusWarenkorb) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	fmt.Println("In Warenkorb arbeitet...")

	/* Session */
	var id int
	w, r, id = handleSession(w, r)

	if id != 0 {

		// acc, _ := model.GetById_Account(id)

		session, _ := store.Get(r, "IdSession")
		q := r.URL.Query()
		itemsWarenkorb, _ := session.Values["Warenkorb"]

		if itemsWarenkorb != nil {

			idItem, _ := strconv.Atoi(q.Get("idItem"))

			warenkorb := []model.Item{}

			var userW = &[]model.Item{}
			userW = itemsWarenkorb.(*[]model.Item)

			for _, obj := range *userW {
				if obj.IdItem != idItem {
					warenkorb = append(warenkorb, obj)
				}
			}

			fmt.Println(warenkorb)

			session.Values["Warenkorb"] = warenkorb
			session.Save(r, w)

			w.Header().Set("Content-Type", "text/html")
			w.WriteHeader(http.StatusOK)
	
			tmpl, _ := template.ParseFiles(config.PfadTemplate + "/feedback/ausWarenkorbRaus.html")
	
			tmpl.ExecuteTemplate(w, "ausWarenkorbRaus", nil)
		}
	} else {
		session, _ := store.Get(r, "IdSession")
		q := r.URL.Query()
		itemsWarenkorb, _ := session.Values["Warenkorb"]

		if itemsWarenkorb != nil {

			idItem, _ := strconv.Atoi(q.Get("idItem"))

			warenkorb := []model.Item{}

			var userW = &[]model.Item{}
			userW = itemsWarenkorb.(*[]model.Item)

			for _, obj := range *userW {
				if obj.IdItem != idItem {
					warenkorb = append(warenkorb, obj)
				}
			}

			fmt.Println(warenkorb)

			session.Values["Warenkorb"] = warenkorb
			session.Save(r, w)

			w.Header().Set("Content-Type", "text/html")
			w.WriteHeader(http.StatusOK)
	
			tmpl, _ := template.ParseFiles(config.PfadTemplate + "/feedback/ausWarenkorbRaus.html")
	
			tmpl.ExecuteTemplate(w, "ausWarenkorbRaus", nil)
		}
	}
}
