package handler

import (
	"borgdir.media/app/model"
	"borgdir.media/config"
	"fmt"
	"html/template"
	"net/http"
	"strconv"
)

type Handler_InWarenkorb struct{}

func (h *Handler_InWarenkorb) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	fmt.Println("In Warenkorb arbeitet...")

	/* Session */
	var id int
	w, r, id = handleSession(w, r)

	if id != 0 {

		// acc, _ := model.GetById_Account(id)

		session, _ := store.Get(r, "IdSession")
		q := r.URL.Query()
		itemsWarenkorb, _ := session.Values["Warenkorb"]

		if itemsWarenkorb == nil {
			fmt.Println("Warenkorb ist leer")

			warenkorb := []model.Item{}

			idItem, _ := strconv.Atoi(q.Get("idItem"))
			item, _ := model.GetById_Item(idItem)

			warenkorb = append(warenkorb, item)

			fmt.Println(warenkorb)

			session.Values["Warenkorb"] = warenkorb
			session.Save(r, w)

			w.Header().Set("Content-Type", "text/html")
			w.WriteHeader(http.StatusOK)
		
			// filePath := config.PfadStatic + "/index.html"
			// head := config.PfadTemplate + "/head.html"
			// header := config.PfadTemplate + "/headerUser.html"
			// footer := config.PfadTemplate + "/footer.html"
			// content := config.PfadTemplate + "/contentIndex.html"

			tmpl, _ := template.ParseFiles(config.PfadTemplate + "/feedback/inWarenkorbHinzu.html")

			tmpl.ExecuteTemplate(w, "inWarenkorbHinzu", nil)
		} else {
			fmt.Println("Warenkorb ist nicht leer!")

			idItem, _ := strconv.Atoi(q.Get("idItem"))
			item, _ := model.GetById_Item(idItem)

			warenkorb := []model.Item{}

			var userW = &[]model.Item{}
			userW = itemsWarenkorb.(*[]model.Item)

			fmt.Println(userW)

			for _, obj := range *userW {
				warenkorb = append(warenkorb, obj)
			}

			warenkorb = append(warenkorb, item)

			fmt.Println(warenkorb)

			session.Values["Warenkorb"] = warenkorb
			session.Save(r, w)

			w.Header().Set("Content-Type", "text/html")
			w.WriteHeader(http.StatusOK)

			// filePath := config.PfadStatic + "/index.html"
			// head := config.PfadTemplate + "/head.html"
			// header := config.PfadTemplate + "/headerUser.html"
			// footer := config.PfadTemplate + "/footer.html"
			// content := config.PfadTemplate + "/contentIndex.html"

			tmpl, _ := template.ParseFiles(config.PfadTemplate + "/feedback/inWarenkorbHinzu.html")

			tmpl.ExecuteTemplate(w, "inWarenkorbHinzu", nil)
		}
	} else {
		session, _ := store.Get(r, "IdSession")
		q := r.URL.Query()
		itemsWarenkorb, _ := session.Values["Warenkorb"]

		if itemsWarenkorb == nil {
			fmt.Println("Warenkorb ist leer")

			warenkorb := []model.Item{}

			idItem, _ := strconv.Atoi(q.Get("idItem"))
			item, _ := model.GetById_Item(idItem)

			warenkorb = append(warenkorb, item)

			fmt.Println(warenkorb)

			session.Values["Warenkorb"] = warenkorb
			session.Save(r, w)

			w.Header().Set("Content-Type", "text/html")
			w.WriteHeader(http.StatusOK)

			// filePath := config.PfadStatic + "/index.html"
			// head := config.PfadTemplate + "/head.html"
			// header := config.PfadTemplate + "/header.html"
			// footer := config.PfadTemplate + "/footer.html"
			// content := config.PfadTemplate + "/contentIndex.html"

			tmpl, _ := template.ParseFiles(config.PfadTemplate + "/feedback/inWarenkorbHinzu.html")

			tmpl.ExecuteTemplate(w, "inWarenkorbHinzu", nil)
		} else {
			fmt.Println("Warenkorb ist nicht leer!")

			idItem, _ := strconv.Atoi(q.Get("idItem"))
			item, _ := model.GetById_Item(idItem)

			warenkorb := []model.Item{}

			var userW = &[]model.Item{}
			userW = itemsWarenkorb.(*[]model.Item)

			fmt.Println(userW)

			for _, obj := range *userW {
				warenkorb = append(warenkorb, obj)
			}

			warenkorb = append(warenkorb, item)

			fmt.Println(warenkorb)

			session.Values["Warenkorb"] = warenkorb
			session.Save(r, w)

			w.Header().Set("Content-Type", "text/html")
			w.WriteHeader(http.StatusOK)

			// filePath := config.PfadStatic + "/index.html"
			// head := config.PfadTemplate + "/head.html"
			// header := config.PfadTemplate + "/header.html"
			// footer := config.PfadTemplate + "/footer.html"
			// content := config.PfadTemplate + "/contentIndex.html"

			tmpl, _ := template.ParseFiles(config.PfadTemplate + "/feedback/inWarenkorbHinzu.html")

			tmpl.ExecuteTemplate(w, "inWarenkorbHinzu", nil)
		}
	}
}
