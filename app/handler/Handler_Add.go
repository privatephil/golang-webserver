package handler

import (
	"borgdir.media/app/model"
	"borgdir.media/config"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strconv" // String to Int Converter
)

type Handler_Add struct{}

func (h *Handler_Add) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Println(r.Method)
	if r.Method == "GET" {
		fmt.Println("Add Handler serving...")

		/* Session */
		var id int
		w, r, id = handleSession(w, r)

		if id != 0 {
			fmt.Println("Nutzer mit ID " + strconv.Itoa(id) + " angemeldet!")

			acc, _ := model.GetById_Account(id)

			if acc.Admin == 1 {
				w.Header().Set("Content-Type", "text/html")
				w.WriteHeader(http.StatusOK)

				filePath := config.PfadAdmin + "/add.html"
				head := config.PfadTemplate + "/head.html"
				header := config.PfadTemplate + "/headerUser.html"
				footer := config.PfadTemplate + "/footer.html"
				content := config.PfadTemplate + "/contentAdd.html"

				tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)

				tmpl.ExecuteTemplate(w, "header", acc)
				tmpl.ExecuteTemplate(w, "add", "")
			} else {
				w.Header().Set("Content-Type", "text/html")
				w.WriteHeader(http.StatusOK)

				filePath := config.PfadStatic + "/index.html"
				head := config.PfadTemplate + "/head.html"
				header := config.PfadTemplate + "/headerUser.html"
				footer := config.PfadTemplate + "/footer.html"
				content := config.PfadTemplate + "/contentIndex.html"

				tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)

				tmpl.ExecuteTemplate(w, "index", acc)
			}

		} else {
			w.Header().Set("Content-Type", "text/html")
			w.WriteHeader(http.StatusOK)

			filePath := config.PfadStatic + "/index.html"
			head := config.PfadTemplate + "/head.html"
			header := config.PfadTemplate + "/header.html"
			footer := config.PfadTemplate + "/footer.html"
			content := config.PfadTemplate + "/contentIndex.html"

			tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)

			tmpl.ExecuteTemplate(w, "index", "")
		}

	} else if r.Method == "POST" {
		fmt.Println("Formular geposted!")
		// r.ParseForm()
		r.ParseMultipartForm(32)
		fmt.Println("bezeichnung: ", r.Form["bezeichnung"])
		fmt.Println("inhalt: ", r.Form["inhalt"])
		fmt.Println("beschreibung: ", r.Form["beschreibung"])
		fmt.Println("kategorie: ", r.Form["kategorie"])
		fmt.Println("lagerort: ", r.Form["lagerort"])
		fmt.Println("anzahl: ", r.Form["anzahl"])

		anzahl, _ := strconv.Atoi(r.Form["anzahl"][0])

		equipment := model.Equipment{
			Name:         r.Form["bezeichnung"][0],
			Beschreibung: r.Form["beschreibung"][0],
			Inhalt:       r.Form["inhalt"][0],
			Picture:      0,
		}

		kategorieEquipment := r.Form["kategorie"][0]

		switch kategorieEquipment {
			case "kameras":
				equipment.IdKategorie = 1
			case "mikrofone":
				equipment.IdKategorie = 2
			case "pa":
				equipment.IdKategorie = 3
			case "zubehoer":
				equipment.IdKategorie = 4
		}

		var errAdd error = nil

		if anzahl > 0 {
			errAdd = equipment.AddEquipment()

			if errAdd == nil {
				equipment, _ = model.GetLastEntry_Equipment()

				item := model.Item{
					IdEquipment: equipment.IdEquipment,
				}

				lagerortItem := r.Form["lagerort"][0]

				switch lagerortItem {
					case "schrank":
						item.IdLagerort = 1
					case "dachlager":
						item.IdLagerort = 2
					case "lager 1":
						item.IdLagerort = 3
					case "lager 2":
						item.IdLagerort = 4
					case "transporter":
						item.IdLagerort = 5
				}

				for i := 0; i < anzahl; i++ {
					item.AddItem()
				}

				/* Wenn Bild vorhanden setze Bild, ansonsten Platzhalter */

				file, _, err := r.FormFile("picture")

				if err != nil {
					fmt.Println("Ein Fehler beim hochladen des Bildes ist aufgetreten!")

					w.Header().Set("Content-Type", "text/html")
					w.WriteHeader(http.StatusOK)

					tmpl, _ := template.ParseFiles(config.PfadTemplate + "/feedback/addingItemFeedbackPositiv.html")

					tmpl.ExecuteTemplate(w, "addingItemFeedbackPositiv", nil)

					return
				}
				defer file.Close()
				fmt.Println("defer file.Close()")
				fileBytes, err := ioutil.ReadAll(file)

				fmt.Println(fileBytes)

				if err != nil || len(fileBytes) == 0 {
					fmt.Println("Ein Fehler beim hochladen des Bildes ist aufgetreten!")

					w.Header().Set("Content-Type", "text/html")
					w.WriteHeader(http.StatusOK)

					tmpl, _ := template.ParseFiles(config.PfadTemplate + "/feedback/addingItemFeedbackPositiv.html")

					tmpl.ExecuteTemplate(w, "addingItemFeedbackPositiv", nil)

					return
				}

				fileName := strconv.Itoa(equipment.IdEquipment) + ".jpg"
				newPath := filepath.Join(config.PfadStatic+"/images/equipment", fileName)

				newFile, _ := os.Create(newPath)
				defer newFile.Close()
				fmt.Println("defer newFile.Close()")
				_, err = newFile.Write(fileBytes)

				if err != nil {
					fmt.Println("Ein Fehler beim hochladen des Bildes ist aufgetreten!")

					w.Header().Set("Content-Type", "text/html")
					w.WriteHeader(http.StatusOK)

					tmpl, _ := template.ParseFiles(config.PfadTemplate + "/feedback/addingItemFeedbackPositiv.html")

					tmpl.ExecuteTemplate(w, "addingItemFeedbackPositiv", nil)

					return
				}

				equipment.Picture = 1
				equipment.AlterEquipment()

				/* --- */
				w.Header().Set("Content-Type", "text/html")
				w.WriteHeader(http.StatusOK)

				tmpl, _ := template.ParseFiles(config.PfadTemplate + "/feedback/addingItemFeedbackPositiv.html")

				tmpl.ExecuteTemplate(w, "addingItemFeedbackPositiv", nil)

			}
		} else {

			w.Header().Set("Content-Type", "text/html")
			w.WriteHeader(http.StatusOK)

			tmpl, _ := template.ParseFiles(config.PfadTemplate + "/feedback/addingItemFeedbackNegativ.html")

			tmpl.ExecuteTemplate(w, "addingItemFeedbackNegativ", nil)

		}

	}
}
