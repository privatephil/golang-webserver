package handler

import (
	"borgdir.media/app/model"
	// "borgdir.media/config"
	"fmt"
	"time"
	// "html/template"
	"net/http"
	"strconv"
)

type Handler_Verlaengern struct{}

func (h *Handler_Verlaengern) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	fmt.Println("Verlängern arbeitet...")

	/* Session */
	var id int
	w, r, id = handleSession(w, r)

	if id != 0 {

		q := r.URL.Query()
		idAusleihe, _ := strconv.Atoi(q.Get("idAusleihe"))
		
		ausleihe, _ := model.GetById_Ausleihe(idAusleihe)
		ausleihe.AusleiheVerlaengern()
		
		/* MeinEquipment laden */
		acc, _ := model.GetById_Account(id)
		
		ausleihenUser, _ := model.GetByUserId_Ausleihe(acc.IdAccount)
		
		ausleiheSpezial := []AusleiheSpezial{}
		
		for _, obj := range ausleihenUser {
			item, _ := model.GetById_Item(obj.IdItem)
			equip, _ := model.GetById_Equipment(item.IdEquipment)
			
			datum, _ := time.Parse(time.RFC3339, obj.Rueckgabe_bis)
			obj.Rueckgabe_bis = datum.Format("02.01.2006")
			datum, _ = time.Parse(time.RFC3339, obj.Entliehen_am)
			obj.Entliehen_am = datum.Format("02.01.2006")
			
			ausl := AusleiheSpezial{
				Ausleihe: obj,
				Item: item,
				Equipment: equip,
			}
			
			ausleiheSpezial = append(ausleiheSpezial, ausl)
		}
		
		/*
		w.Header().Set("Content-Type", "text/html")
		w.WriteHeader(http.StatusOK)

		filePath := config.PfadHtml + "/my-equipment.html"
		head := config.PfadTemplate + "/head.html"
		header := config.PfadTemplate + "/header.html"
		footer := config.PfadTemplate + "/footer.html"
		content := config.PfadTemplate + "/contentMyEquipment.html"

		tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)

		tmpl.ExecuteTemplate(w, "myEquipment", ausleiheSpezial)
		*/
		
		handlerMyEquipment := Handler_MeinEquipment{}
		handlerMyEquipment.ServeHTTP(w, r)
		
	} else {
		// Feedback nicht angemeldete Benutzer dürfen nichts ausleihen
		fmt.Println("Benutzer ist nicht angemeldet...")
	}
}
