package handler

import (
	"borgdir.media/app/model"
	"borgdir.media/config"
	"fmt"
	"html/template"
	"net/http"
)

type Handler_DeleteProfil struct{}

func (h *Handler_DeleteProfil) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {

		/* Session */
		var id int
		w, r, id = handleSession(w, r)

		if id != 0 {
			acc, _ := model.GetById_Account(id)

			acc.DeleteAccount()

			fmt.Println("Account gelöscht!")

			w, r = handleLogoutUser(w, r)
			
			w.Header().Set("Content-Type", "text/html")
			w.WriteHeader(http.StatusOK)

			tmpl, _ := template.ParseFiles(config.PfadTemplate + "/feedback/accountGeloescht.html")

			tmpl.ExecuteTemplate(w, "accountGeloescht", nil)
		}
	}
}
