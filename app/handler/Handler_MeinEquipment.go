package handler

import (
	"borgdir.media/config"
	"borgdir.media/app/model"
	"fmt"
	"time"
	"html/template"
	"net/http"
	"strconv"
)

type Handler_MeinEquipment struct{}

type AusleiheSpezial struct {
	Ausleihe		model.Ausleihe
	Item			model.Item
	Equipment		model.Equipment
}

type VormerkungSpezial struct {
	Vormerkung		model.Vormerkung
	Item			model.Item
	Equipment		model.Equipment
}

func (h *Handler_MeinEquipment) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Mein Equipment Handler serving...")

	/* Session */
	var id int
	w, r, id = handleSession(w, r)

	if id != 0 {
		fmt.Println("Nutzer mit ID " + strconv.Itoa(id) + " angemeldet!")
		
		acc, _ := model.GetById_Account(id)
		
		ausleihenUser, _ := model.GetByUserId_Ausleihe(acc.IdAccount)
		
		ausleiheSpezial := []AusleiheSpezial{}
		
		for _, obj := range ausleihenUser {
			item, _ := model.GetById_Item(obj.IdItem)
			equip, _ := model.GetById_Equipment(item.IdEquipment)
			
			datum, _ := time.Parse(time.RFC3339, obj.Rueckgabe_bis)
			obj.Rueckgabe_bis = datum.Format("02.01.2006")
			datum, _ = time.Parse(time.RFC3339, obj.Entliehen_am)
			obj.Entliehen_am = datum.Format("02.01.2006")
			
			ausl := AusleiheSpezial{
				Ausleihe: obj,
				Item: item,
				Equipment: equip,
			}
			
			ausleiheSpezial = append(ausleiheSpezial, ausl)
		}
		
		vormerkungenUser, _ := model.GetByUserId_Vormerkung(acc.IdAccount)
		vormerkungSpezial := []VormerkungSpezial{}
		
		for _, obj := range vormerkungenUser {
			item, _ := model.GetById_Item(obj.IdItem)
			equip, _ := model.GetById_Equipment(item.IdEquipment)
			
			datum, _ := time.Parse(time.RFC3339, obj.Rueckgabe)
			obj.Rueckgabe = datum.Format("02.01.2006")
			
			vorm := VormerkungSpezial{
				Vormerkung: obj,
				Item: item,
				Equipment: equip,
			}
			
			vormerkungSpezial = append(vormerkungSpezial, vorm)
		}
		
		w.Header().Set("Content-Type", "text/html")
		w.WriteHeader(http.StatusOK)

		filePath := config.PfadHtml + "/my-equipment.html"
		head := config.PfadTemplate + "/head.html"
		header := config.PfadTemplate + "/headerUser.html"
		footer := config.PfadTemplate + "/footer.html"
		content := config.PfadTemplate + "/contentMyEquipment.html"
		contentVormerkungen := config.PfadTemplate + "/contentMyEquipmentVormerkungen.html"

		tmpl, _ := template.ParseFiles(filePath, head, header, content, contentVormerkungen, footer)

		tmpl.ExecuteTemplate(w, "header", acc)
		tmpl.ExecuteTemplate(w, "myEquipment", ausleiheSpezial)
		tmpl.ExecuteTemplate(w, "contentMyEquipmentVormerkungen", vormerkungSpezial)
		// tmpl.ExecuteTemplate(w, "footer", nil)
		
	} else {
		w.Header().Set("Content-Type", "text/html")
		w.WriteHeader(http.StatusOK)

		filePath := config.PfadHtml + "/my-equipment.html"
		head := config.PfadTemplate + "/head.html"
		header := config.PfadTemplate + "/header.html"
		footer := config.PfadTemplate + "/footer.html"
		content := config.PfadTemplate + "/contentMyEquipment.html"

		tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)

		tmpl.ExecuteTemplate(w, "myEquipment", "")
	}

}
