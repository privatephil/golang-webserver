package handler

import (
	"borgdir.media/app/model"
	"borgdir.media/config"
	"fmt"
	"html/template"
	"net/http"
	"strconv"
	"time"
)

type Handler_Ausleihen struct{}

func (h *Handler_Ausleihen) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	fmt.Println("Ausleihen arbeitet...")

	/* Session */
	var id int
	w, r, id = handleSession(w, r)

	if id != 0 {

		fmt.Println("Benutzer ist angemeldet und kann das Equipment ausleihen...")

		/* Zeit */
		t := time.Now()
		fmt.Println("Aktuelle Zeit geladen")

		acc, _ := model.GetById_Account(id)
		fmt.Println("Account geladen: " + strconv.Itoa(id))

		session, _ := store.Get(r, "IdSession")
		itemsWarenkorb, _ := session.Values["Warenkorb"]
		fmt.Println("Warenkorb aus Session geladen")

		// idItem, _ := strconv.Atoi(q.Get("idItem"))
		// item, _ := model.GetById_Item(idItem)

		warenkorb := []model.Item{}

		var userW = &[]model.Item{}
		userW = itemsWarenkorb.(*[]model.Item)

		ausleiheAll, _ := model.GetAll_Ausleihe()

		vorhanden := false
		for _, obj := range *userW {

			fmt.Println( "Nächstes Objekt: " + strconv.Itoa(obj.IdEquipment) )

			for _, ausl := range ausleiheAll {

				fmt.Println( "Nächstes Ausleihe: " + strconv.Itoa(ausl.IdAusleihe) )

				if obj.IdItem == ausl.IdItem {
					/* Wenn Item bereits von jemandem Ausgeliehen wird es nicht der Ausleihe hinzugefügt aber bleibt im Warenkorb erhalten */
					// warenkorb = append(warenkorb, obj)
					fmt.Println("Ist bereits verliehen. Wird vorgemerkt!")
					
					/* Wenn Item bereits von jemandem Ausgeliehen wird es vorgemerkt */
					i, _ := model.GetById_Item(obj.IdItem)
					
					vormerkung := model.Vormerkung{
						IdEquipment:	i.IdEquipment,
						IdAccount:		acc.IdAccount,
						Rueckgabe:		ausl.Rueckgabe_bis,
						IdItem:			obj.IdItem,
					}
					
					vormerkung.AddVormerkung()
					
					vorhanden = true
					break
				}

			}
			if vorhanden == false {
				fmt.Println("Ist nicht verliehen. Wird hinzugefügt!")
				ausleihe := model.Ausleihe{
					IdItem:        obj.IdItem,
					IdAccount:     acc.IdAccount,
					Entliehen_am:  t.Format(time.RFC3339),
					Rueckgabe_bis: t.AddDate(0, 0, 14).Format(time.RFC3339),
				}

				ausleihe.AddAusleihe()
				
				item, _ := model.GetById_Item(obj.IdItem)
				item.Entliehen = 1;
				item.AlterItem()

				
				t = time.Now()
			}
			vorhanden = false
		}

		session.Values["Warenkorb"] = warenkorb
		session.Save(r, w)
		
		/*
		w.Header().Set("Content-Type", "text/html")
		w.WriteHeader(http.StatusOK)

		filePath := config.PfadStatic + "/index.html"
		head := config.PfadTemplate + "/head.html"
		header := config.PfadTemplate + "/headerUser.html"
		footer := config.PfadTemplate + "/footer.html"
		content := config.PfadTemplate + "/contentIndex.html"

		tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)

		tmpl.ExecuteTemplate(w, "index", acc)
		*/
		
		w.Header().Set("Content-Type", "text/html")
		w.WriteHeader(http.StatusOK)

		tmpl, _ := template.ParseFiles(config.PfadTemplate + "/feedback/ausleihenFeedbackPositiv.html")

		tmpl.ExecuteTemplate(w, "ausleihenFeedbackPositiv", nil)

	} else {
		// Feedback nicht angemeldete Benutzer dürfen nichts ausleihen
		fmt.Println("Benutzer ist nicht angemeldet...")
		
		w.Header().Set("Content-Type", "text/html")
		w.WriteHeader(http.StatusOK)

		tmpl, _ := template.ParseFiles(config.PfadTemplate + "/feedback/ausleihenFeedbackNegativ.html")

		tmpl.ExecuteTemplate(w, "ausleihenFeedbackNegativ", nil)
	}
}
