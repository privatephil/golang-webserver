package handler

import (
	"borgdir.media/app/model"
	"borgdir.media/config"
	"fmt"
	"html/template"
	"net/http"
	"strconv"
	"golang.org/x/crypto/bcrypt"
)

type Handler_EditClient struct{}

func (h *Handler_EditClient) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Benutzerverwaltung Handler serving...")

	if r.Method == "GET" {

		/* Session */
		var id int
		w, r, id = handleSession(w, r)

		if id != 0 {

			acc1, _ := model.GetById_Account(id)

			if acc1.Admin == 1 {

				q := r.URL.Query()
				s := q.Encode()

				fmt.Println(s)

				id, _ := strconv.Atoi(q.Get("idAccount"))

				acc, _ := model.GetById_Account(id)

				fmt.Println("Benutzername: " + acc.Benutzername)
				fmt.Println("Email: " + acc.Email)
				fmt.Println("Aktiv_bis: " + acc.Aktiv_bis)

				w.Header().Set("Content-Type", "text/html")
				w.WriteHeader(http.StatusOK)

				filePath := config.PfadAdmin + "/edit-client.html"
				head := config.PfadTemplate + "/head.html"
				header := config.PfadTemplate + "/headerUser.html"
				footer := config.PfadTemplate + "/footer.html"
				content := config.PfadTemplate + "/contentEditClients.html"

				tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)
				
				tmpl.ExecuteTemplate(w, "header", acc1)
				tmpl.ExecuteTemplate(w, "editClients", acc)
			} else {
				w.Header().Set("Content-Type", "text/html")
				w.WriteHeader(http.StatusOK)

				filePath := config.PfadStatic + "/index.html"
				head := config.PfadTemplate + "/head.html"
				header := config.PfadTemplate + "/headerUser.html"
				footer := config.PfadTemplate + "/footer.html"
				content := config.PfadTemplate + "/contentIndex.html"

				tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)

				tmpl.ExecuteTemplate(w, "index", acc1)
			}
		} else {
			w.Header().Set("Content-Type", "text/html")
			w.WriteHeader(http.StatusOK)

			filePath := config.PfadStatic + "/index.html"
			head := config.PfadTemplate + "/head.html"
			header := config.PfadTemplate + "/header.html"
			footer := config.PfadTemplate + "/footer.html"
			content := config.PfadTemplate + "/contentIndex.html"

			tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)

			tmpl.ExecuteTemplate(w, "index", "")
		}
	} else if r.Method == "POST" {
		
		fmt.Println("Edit Client POST")

		/* Session */
		var id int
		w, r, id = handleSession(w, r)

		if id != 0 {

			acc, _ := model.GetById_Account(id)

			if acc.Admin == 1 {

				r.ParseForm()

				if r.Form["password"][0] == r.Form["pwdrepeat"][0] {

					idKunde, _ := strconv.Atoi(r.Form["idKunde"][0])

					accKunde, _ := model.GetById_Account(idKunde)
					
					bytes, _ := bcrypt.GenerateFromPassword([]byte(r.Form["password"][0]), 12)
					
					newAcc := model.Account{
						IdAccount:    accKunde.IdAccount,
						Benutzername: r.Form["username"][0],
						Passwort:     string(bytes),
						Email:        r.Form["mail"][0],
						Admin:        accKunde.Admin,
						Gesperrt:     accKunde.Gesperrt,
						Aktiv_bis:    accKunde.Aktiv_bis,
					}

					newAcc.AlterAccount()
					
					fmt.Println("Account erfolgreich geändert!")
					
					w.Header().Set("Content-Type", "text/html")
					w.WriteHeader(http.StatusOK)
					
					tmpl, _ := template.ParseFiles(config.PfadTemplate + "/feedback/clientGeaendert.html")
					
					tmpl.ExecuteTemplate(w, "clientGeaendert", nil)
				} else {
					w.Header().Set("Content-Type", "text/html")
					w.WriteHeader(http.StatusOK)
					
					tmpl, _ := template.ParseFiles(config.PfadTemplate + "/feedback/clientGeaendertPasswoerterFalsch.html")
					
					tmpl.ExecuteTemplate(w, "clientGeaendertPasswoerterFalsch", nil)
				}
			}
		}
	}
}
