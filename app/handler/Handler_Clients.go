package handler

import (
	"borgdir.media/app/model"
	"borgdir.media/config"
	"fmt"
	"html/template"
	"net/http"
	"strconv"
)

type Handler_Clients struct{}

type AccountSpezial struct {
	Account 	model.Account
	Items		[]model.Item
	Equipments 	[]model.Equipment
}

func (h *Handler_Clients) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Clients Handler serving...")
	
	/* Session */
	var id int
	w, r, id = handleSession(w, r)
	
	if id != 0 {
		fmt.Println("Nutzer mit ID " + strconv.Itoa(id) + " angemeldet!")
		
		acc, _ := model.GetById_Account(id)
			
		if acc.Admin == 1 {
			
			q := r.URL.Query()
			search := q.Get("search")
			kategorie := q.Get("kategorie")
			
			// accounts, _ := model.GetAll_Accounts()
			accounts, _ := model.Search_Accounts(search, kategorie)
			ausleihen, _ := model.GetAll_Ausleihe()
		
			var accSpezials []AccountSpezial
		
			for _, acc := range accounts {
				
				obj := AccountSpezial {
					Account: acc,
				}
				
				for _, ausl := range ausleihen {
					if acc.IdAccount == ausl.IdAccount {
						item, _ := model.GetById_Item(ausl.IdItem)
						obj.Items = append(obj.Items, item)
						
						equip, _ := model.GetById_Equipment(item.IdEquipment)
						obj.Equipments = append(obj.Equipments, equip)
					}
				}
				
				accSpezials = append(accSpezials, obj)
			}
			
			w.Header().Set("Content-Type", "text/html")
			w.WriteHeader(http.StatusOK)
		
			// accounts, _ := model.GetAllAccounts()
		
			filePath := config.PfadAdmin + "/clients.html"
			head := config.PfadTemplate + "/head.html"
			header := config.PfadTemplate + "/headerUser.html"
			footer := config.PfadTemplate + "/footer.html"
			content := config.PfadTemplate + "/contentClients.html"
		
			tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)
			
			tmpl.ExecuteTemplate(w, "header", acc)
			tmpl.ExecuteTemplate(w, "clients", accSpezials)
		} else {
			w.Header().Set("Content-Type", "text/html")
			w.WriteHeader(http.StatusOK)
			
			filePath 	:= config.PfadStatic + "/index.html"
			head		:= config.PfadTemplate + "/head.html"
			header 		:= config.PfadTemplate + "/headerUser.html"
			footer 		:= config.PfadTemplate + "/footer.html"
			content		:= config.PfadTemplate + "/contentIndex.html"
			
			tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)
			
			tmpl.ExecuteTemplate(w, "index", acc)
		} 
	} else {
		w.Header().Set("Content-Type", "text/html")
		w.WriteHeader(http.StatusOK)
		
		filePath 	:= config.PfadStatic + "/index.html"
		head		:= config.PfadTemplate + "/head.html"
		header 		:= config.PfadTemplate + "/header.html"
		footer 		:= config.PfadTemplate + "/footer.html"
		content		:= config.PfadTemplate + "/contentIndex.html"
		
		tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)
		
		tmpl.ExecuteTemplate(w, "index", "")
	}
	
	
}
