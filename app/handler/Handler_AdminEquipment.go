package handler

import (
	"borgdir.media/app/model"
	"borgdir.media/config"
	"fmt"
	"time"
	"html/template"
	"net/http"
	"strconv"
)

type Handler_AdminEquipment struct{}

type EquipmentSpezial struct {
	Item      model.Item
	Equipment model.Equipment
	Lagerort  model.Lagerort
	Ausleihe  model.Ausleihe
	Account   model.Account
}

func (h *Handler_AdminEquipment) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Equipmentverwaltung Handler serving...")

	/* Session */
	var id int
	w, r, id = handleSession(w, r)

	if id != 0 {
		fmt.Println("Nutzer mit ID " + strconv.Itoa(id) + " angemeldet!")

		acc1, _ := model.GetById_Account(id)

		if acc1.Admin == 1 {
			// items, _ := model.GetAll_Item()
			
			q := r.URL.Query()
			search := q.Get("search")
			kategorie := q.Get("kategorie")
			
			items, _ := model.Search_Items(search, kategorie, "")
			ausleihe, _ := model.GetAll_Ausleihe()

			equipmentSpezial := []EquipmentSpezial{}

			for _, item := range items {

				var acc model.Account
				var ausl2 model.Ausleihe

				for _, ausl := range ausleihe {
					if ausl.IdItem == item.IdItem {
						acc, _ = model.GetById_Account(ausl.IdAccount)
						ausl2 = ausl
						
						datum, _ := time.Parse(time.RFC3339, ausl2.Rueckgabe_bis)
						ausl2.Rueckgabe_bis = datum.Format("02.01.2006")
					}
				}

				account := acc
				lagerort, _ := model.GetById_Lagerort(item.IdLagerort)
				equipment, _ := model.GetById_Equipment(item.IdEquipment)

				var obj EquipmentSpezial

				if ausl2.IdAusleihe > 0 {
					obj = EquipmentSpezial{
						Item:      item,
						Equipment: equipment,
						Lagerort:  lagerort,
						Ausleihe:  ausl2,
						Account:   account,
					}
					
				} else {
					obj = EquipmentSpezial{
						Item:      item,
						Equipment: equipment,
						Lagerort:  lagerort,
						Account:   account,
					}
				}

				equipmentSpezial = append(equipmentSpezial, obj)
			}

			w.Header().Set("Content-Type", "text/html")
			w.WriteHeader(http.StatusOK)

			filePath := config.PfadAdmin + "/equipment.html"
			head := config.PfadTemplate + "/head.html"
			header := config.PfadTemplate + "/headerUser.html"
			footer := config.PfadTemplate + "/footer.html"
			content := config.PfadTemplate + "/contentAdminEquipment.html"

			tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)

			tmpl.ExecuteTemplate(w, "header", acc1)
			tmpl.ExecuteTemplate(w, "adminEquipment", equipmentSpezial)
		} else {
			w.Header().Set("Content-Type", "text/html")
			w.WriteHeader(http.StatusOK)
			
			filePath 	:= config.PfadStatic + "/index.html"
			head		:= config.PfadTemplate + "/head.html"
			header 		:= config.PfadTemplate + "/headerUser.html"
			footer 		:= config.PfadTemplate + "/footer.html"
			content		:= config.PfadTemplate + "/contentIndex.html"
			
			tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)
			
			tmpl.ExecuteTemplate(w, "index", acc1)
		}
	} else {
		w.Header().Set("Content-Type", "text/html")
		w.WriteHeader(http.StatusOK)
		
		filePath 	:= config.PfadStatic + "/index.html"
		head		:= config.PfadTemplate + "/head.html"
		header 		:= config.PfadTemplate + "/header.html"
		footer 		:= config.PfadTemplate + "/footer.html"
		content		:= config.PfadTemplate + "/contentIndex.html"
		
		tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)
		
		tmpl.ExecuteTemplate(w, "index", "")
	}

}
