package handler

import (
	"borgdir.media/app/model"
	"borgdir.media/config"
	"fmt"
	"html/template"
	"net/http"
	"strconv"
	"golang.org/x/crypto/bcrypt"
)

type Handler_Profil struct{}

func (h *Handler_Profil) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	fmt.Println("Profil Handler serving...")

	if r.Method == "GET" {
		/* Session */
		var id int
		w, r, id = handleSession(w, r)

		if id != 0 {
			fmt.Println("Nutzer mit ID " + strconv.Itoa(id) + " angemeldet!")

			acc, _ := model.GetById_Account(id)

			w.Header().Set("Content-Type", "text/html")
			w.WriteHeader(http.StatusOK)

			filePath := config.PfadHtml + "/profil.html"
			head := config.PfadTemplate + "/head.html"
			header := config.PfadTemplate + "/headerUser.html"
			footer := config.PfadTemplate + "/footer.html"
			content := config.PfadTemplate + "/contentProfil.html"

			tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)

			tmpl.ExecuteTemplate(w, "profil", acc)
		} else {
			w.Header().Set("Content-Type", "text/html")
			w.WriteHeader(http.StatusOK)

			filePath := config.PfadStatic + "/index.html"
			head := config.PfadTemplate + "/head.html"
			header := config.PfadTemplate + "/header.html"
			footer := config.PfadTemplate + "/footer.html"
			content := config.PfadTemplate + "/contentIndex.html"

			tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)

			tmpl.ExecuteTemplate(w, "index", "")
		}
	} else if r.Method == "POST" {

		/* Session */
		var id int
		w, r, id = handleSession(w, r)

		if id != 0 {
			acc, _ := model.GetById_Account(id)

			r.ParseForm()
			
			err := bcrypt.CompareHashAndPassword([]byte(acc.Passwort), []byte(r.Form["password-old"][0]))
			
			if err == nil {

				if r.Form["password-new"][0] == r.Form["pwdrepeat"][0] {
					
					bytes, _ := bcrypt.GenerateFromPassword([]byte(r.Form["password-new"][0]), 12)
					
					newAcc := model.Account{
						IdAccount:    id,
						Benutzername: r.Form["username"][0],
						Passwort:     string(bytes),
						Email:        r.Form["mail"][0],
						Admin:        acc.Admin,
						Gesperrt:     acc.Gesperrt,
						Aktiv_bis:    acc.Aktiv_bis,
					}
					
					fmt.Println("---Änderungen---")
					fmt.Println(newAcc.IdAccount)
					fmt.Println(newAcc.Benutzername)
					fmt.Println(newAcc.Passwort)
					fmt.Println(newAcc.Email)
					fmt.Println(newAcc.Admin)
					fmt.Println(newAcc.Gesperrt)
					fmt.Println(newAcc.Aktiv_bis)
					
					newAcc.AlterAccount()
					fmt.Println("Account geändert!")

		
					w.Header().Set("Content-Type", "text/html")
					w.WriteHeader(http.StatusOK)
					
					tmpl, _ := template.ParseFiles(config.PfadTemplate + "/feedback/accountGeaendert.html")
					
					tmpl.ExecuteTemplate(w, "accountGeaendert", nil)
					
				} else {
					fmt.Println("Passwörter unterschiedlich!")
					
					w.Header().Set("Content-Type", "text/html")
					w.WriteHeader(http.StatusOK)
					
					tmpl, _ := template.ParseFiles(config.PfadTemplate + "/feedback/accountAendernPasswoerterUnterschied.html")
					
					tmpl.ExecuteTemplate(w, "accountAendernPasswoerterUnterschied", nil)
				}

			} else {
				fmt.Println("Altes Passwort ist falsch!")
				
				w.Header().Set("Content-Type", "text/html")
				w.WriteHeader(http.StatusOK)
				
				tmpl, _ := template.ParseFiles(config.PfadTemplate + "/feedback/accountAendernPasswortFalsch.html")
				
				tmpl.ExecuteTemplate(w, "accountAendernPasswortFalsch", nil)
			}
		}

	}

}
