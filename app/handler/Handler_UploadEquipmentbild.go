package handler

import (
	"borgdir.media/app/model"
	"borgdir.media/config"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strconv" // String to Int Converter
)

type Handler_UploadEquipmentbild struct{}

func (h *Handler_UploadEquipmentbild) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	fmt.Println("Upload Equipmentbild")

	/* Session */
	var id int
	w, r, id = handleSession(w, r)

	if id != 0 {
		fmt.Println("Nutzer erkannt")
		r.ParseMultipartForm(32)

		file, _, err := r.FormFile("picture")

		if err != nil {
			fmt.Println("Ein Fehler beim hochladen des Bildes ist aufgetreten!")
			return
		}
		defer file.Close()
		fmt.Println("defer file.Close()")
		fileBytes, err := ioutil.ReadAll(file)

		// fmt.Println(fileBytes)

		if err != nil || len(fileBytes) == 0 {
			fmt.Println("Ein Fehler beim hochladen des Bildes ist aufgetreten!")
			return
		}

		idEquipment := r.Form["idEquipment"][0]
		fileName := idEquipment + ".jpg"
		newPath := filepath.Join(config.PfadStatic+"/images/equipment", fileName)

		newFile, _ := os.Create(newPath)
		defer newFile.Close()
		fmt.Println("defer newFile.Close()")
		_, err = newFile.Write(fileBytes)

		if err != nil {
			fmt.Println("Ein Fehler beim hochladen des Bildes ist aufgetreten!")
			return
		}

		convertedId, _ := strconv.Atoi(idEquipment)

		equip, _ := model.GetById_Equipment(convertedId)
		equip.Picture = 1

		err = equip.AlterEquipment()

		if err != nil {
			fmt.Println("Equipment konnte nicht abgeändert werden!")
			return
		}

		/*
			account, _ := model.GetById_Account()
			account.Picture = 1

			err = account.AlterAccount()

			if err != nil {
				fmt.Println("Account konnte nicht abgeändert werden!")
				return
			}

			w.Header().Set("Content-Type", "text/html")
			w.WriteHeader(http.StatusOK)

			filePath := config.PfadHtml + "/profil.html"
			head := config.PfadTemplate + "/head.html"
			header := config.PfadTemplate + "/headerUser.html"
			footer := config.PfadTemplate + "/footer.html"
			content := config.PfadTemplate + "/contentProfil.html"

			tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)

			tmpl.ExecuteTemplate(w, "profil", account)
		*/

		acc1, _ := model.GetById_Account(id)

		items, _ := model.GetAll_Item()
		ausleihe, _ := model.GetAll_Ausleihe()

		equipmentSpezial := []EquipmentSpezial{}

		for _, item := range items {

			var acc model.Account
			var ausl2 model.Ausleihe

			for _, ausl := range ausleihe {
				if ausl.IdItem == item.IdItem {
					acc, _ = model.GetById_Account(ausl.IdAccount)
					ausl2 = ausl
				}
			}

			account := acc
			lagerort, _ := model.GetById_Lagerort(item.IdLagerort)
			equipment, _ := model.GetById_Equipment(item.IdEquipment)

			var obj EquipmentSpezial

			if ausl2.IdAusleihe > 0 {
				obj = EquipmentSpezial{
					Item:      item,
					Equipment: equipment,
					Lagerort:  lagerort,
					Ausleihe:  ausl2,
					Account:   account,
				}

			} else {
				obj = EquipmentSpezial{
					Item:      item,
					Equipment: equipment,
					Lagerort:  lagerort,
					Account:   account,
				}
			}

			equipmentSpezial = append(equipmentSpezial, obj)
		}

		w.Header().Set("Content-Type", "text/html")
		w.WriteHeader(http.StatusOK)

		filePath := config.PfadAdmin + "/equipment.html"
		head := config.PfadTemplate + "/head.html"
		header := config.PfadTemplate + "/headerUser.html"
		footer := config.PfadTemplate + "/footer.html"
		content := config.PfadTemplate + "/contentAdminEquipment.html"

		tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)

		tmpl.ExecuteTemplate(w, "header", acc1)
		tmpl.ExecuteTemplate(w, "adminEquipment", equipmentSpezial)
	}
}
