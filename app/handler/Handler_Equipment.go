package handler

import (
	"borgdir.media/app/model"
	"borgdir.media/config"
	"fmt"
	"html/template"
	"net/http"
)

type Handler_Equipment struct{}

type ItemSpezial struct {
	Item      model.Item
	Equipment model.Equipment
}

func (h *Handler_Equipment) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Equipment Handler serving...")

	/* Session */
	var id int
	w, r, id = handleSession(w, r)

	q := r.URL.Query()
	search := q.Get("search")
	kategorie := q.Get("kategorie")
	sortby := q.Get("sortieren-nach")

	fmt.Println("Suche nach: " + search)
	fmt.Println("Kategorie: " + kategorie)
	fmt.Println("Sort by: " + sortby)

	if id != 0 {

		acc, _ := model.GetById_Account(id)

		w.Header().Set("Content-Type", "text/html")
		w.WriteHeader(http.StatusOK)

		filePath := config.PfadHtml + "/equipment.html"
		head := config.PfadTemplate + "/head.html"
		header := config.PfadTemplate + "/headerUser.html"
		footer := config.PfadTemplate + "/footer.html"
		content := config.PfadTemplate + "/contentEquipment.html"

		itemsSpezial := []ItemSpezial{}

		items, _ := model.Search_Items(search, kategorie, sortby)

		for _, item := range items {

			equip, _ := model.GetById_Equipment(item.IdEquipment)

			obj := ItemSpezial{
				Item:      item,
				Equipment: equip,
			}

			itemsSpezial = append(itemsSpezial, obj)
		}

		/*
			else {
				items, _ := model.GetAll_Item()

				// itemsSpezial = []ItemSpezial{}

				for _, item := range items {

					equip, _ := model.GetById_Equipment(item.IdEquipment)

					obj := ItemSpezial{
						Item:      item,
						Equipment: equip,
					}

					itemsSpezial = append(itemsSpezial, obj)
				}
			}
		*/

		tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)

		tmpl.ExecuteTemplate(w, "header", acc)
		tmpl.ExecuteTemplate(w, "equipment", itemsSpezial)

	} else {
		w.Header().Set("Content-Type", "text/html")
		w.WriteHeader(http.StatusOK)

		filePath := config.PfadHtml + "/equipment.html"
		head := config.PfadTemplate + "/head.html"
		header := config.PfadTemplate + "/header.html"
		footer := config.PfadTemplate + "/footer.html"
		content := config.PfadTemplate + "/contentEquipment.html"

		itemsSpezial := []ItemSpezial{}

		items, _ := model.Search_Items(search, kategorie, sortby)

		for _, item := range items {

			equip, _ := model.GetById_Equipment(item.IdEquipment)

			obj := ItemSpezial{
				Item:      item,
				Equipment: equip,
			}

			itemsSpezial = append(itemsSpezial, obj)
		}

		tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)

		tmpl.ExecuteTemplate(w, "header", "")
		tmpl.ExecuteTemplate(w, "equipment", itemsSpezial)
	}
}
