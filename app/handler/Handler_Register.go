package handler

import (
	"fmt"
	"html/template"
	"net/http"
	"borgdir.media/config"
	"borgdir.media/app/model"
	"strconv"
	"time"
	"golang.org/x/crypto/bcrypt"
)

type Handler_Register struct {}

func (h *Handler_Register) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	
	fmt.Println(r.Method)
	
	if r.Method == "GET" {
		fmt.Println("Register Handler serving...")
		
		/* Session */
		var id int
		w, r, id = handleSession(w, r)
		
		if id != 0 {
			fmt.Println("Nutzer mit ID " + strconv.Itoa(id) + " angemeldet!")
		}
		
		w.Header().Set("Content-Type", "text/html")
		w.WriteHeader(http.StatusOK)
		
		filePath := config.PfadHtml + "/register.html"
		head		:= config.PfadTemplate + "/head.html"
		header 		:= config.PfadTemplate + "/header.html"
		footer 		:= config.PfadTemplate + "/footer.html"
		content		:= config.PfadTemplate + "/contentRegister.html"
		
		tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)
		
		tmpl.ExecuteTemplate(w, "register", "")
	} else if r.Method == "POST" {
		fmt.Println("Formular geposted!")
		r.ParseForm()
		fmt.Println("username: ", r.Form["username"])
		fmt.Println("email: ", r.Form["mail"])
		fmt.Println("pwd: ", r.Form["password"])
		fmt.Println("pwdrepeat: ", r.Form["pwdrepeat"])

		// Überprüfen ob wiederholtes Passwort richtig
		if( r.Form["password"][0] == r.Form["pwdrepeat"][0] ) {
			fmt.Println("Wiederholtes Passwort ist richtig!")
			
			t := time.Now()
			t = t.AddDate(1, 0, 0)
			
			bytes, _ := bcrypt.GenerateFromPassword([]byte(r.Form["password"][0]), 12)
			
			account := model.Account{
				Benutzername: r.Form["username"][0],
				Passwort: string(bytes),
				Email: r.Form["mail"][0],
				Aktiv_bis: t.Format("02.01.2006"),
			}
			
			err := account.AddAccount()
			fmt.Println(err)
			
			
			w.Header().Set("Content-Type", "text/html")
			w.WriteHeader(http.StatusOK)

			tmpl, _ := template.ParseFiles(config.PfadTemplate + "/feedback/registerFeedbackPositiv.html")

			tmpl.ExecuteTemplate(w, "feedbackRegisterPositiv", "")
			
		} else {
			fmt.Println("Wiederholtes Passwort ist falsch!")
			
			w.Header().Set("Content-Type", "text/html")
			w.WriteHeader(http.StatusOK)

			tmpl, _ := template.ParseFiles(config.PfadTemplate + "/feedback/registerFeedbackNegativ.html")

			tmpl.ExecuteTemplate(w, "feedbackRegisterNegativ", "")
		}
		
	}
}