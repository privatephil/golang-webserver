package handler

import (
	"borgdir.media/config"
	"html/template"
	"net/http"
	"fmt"
)

type Handler_Logout struct{}

func (h *Handler_Logout) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	/* Session */
	var id int
	w, r, id = handleSession(w, r)

	if id != 0 {
		fmt.Println("Nutzer wird abgemeldet")
		w, r = handleLogoutUser(w, r)
		
		w.Header().Set("Content-Type", "text/html")
		w.WriteHeader(http.StatusOK)

		filePath := config.PfadStatic + "/index.html"
		head := config.PfadTemplate + "/head.html"
		header := config.PfadTemplate + "/header.html"
		footer := config.PfadTemplate + "/footer.html"
		content := config.PfadTemplate + "/contentIndex.html"

		tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)

		tmpl.ExecuteTemplate(w, "index", "")
	} else {
		w.Header().Set("Content-Type", "text/html")
		w.WriteHeader(http.StatusOK)

		filePath := config.PfadStatic + "/index.html"
		head := config.PfadTemplate + "/head.html"
		header := config.PfadTemplate + "/header.html"
		footer := config.PfadTemplate + "/footer.html"
		content := config.PfadTemplate + "/contentIndex.html"

		tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)

		tmpl.ExecuteTemplate(w, "index", "")
	}
}
