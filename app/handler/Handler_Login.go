package handler

import (
	"borgdir.media/app/model"
	"borgdir.media/config"
	"fmt"
	"html/template"
	"net/http"
	"strconv"
	"golang.org/x/crypto/bcrypt"
)

type Handler_Login struct{}

func (h *Handler_Login) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Println(r.Method)
	
	/* Session */
	var id int
	w, r, id = handleSession(w, r)
	
	if r.Method == "GET" {
		fmt.Println("Login Handler serving...")

		if id != 0 {
			fmt.Println("Nutzer mit ID " + strconv.Itoa(id) + " angemeldet!")
		}

		w.Header().Set("Content-Type", "text/html")
		w.WriteHeader(http.StatusOK)

		filePath := config.PfadHtml + "/login.html"
		head := config.PfadTemplate + "/head.html"
		header := config.PfadTemplate + "/header.html"
		footer := config.PfadTemplate + "/footer.html"
		content := config.PfadTemplate + "/contentLogin.html"

		tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)

		tmpl.ExecuteTemplate(w, "login", "")
	} else if r.Method == "POST" {
		fmt.Println("Formular geposted!")
		
		r.ParseForm()
		fmt.Println("username: ", r.Form["username"])
		fmt.Println("password: ", r.Form["password"])

		account, _ := model.GetByUsername_Account(r.Form["username"][0])

		err := bcrypt.CompareHashAndPassword([]byte(account.Passwort), []byte(r.Form["password"][0]))
		
		if err == nil {

			fmt.Println("Benutzer angemeldet!")

			w, r = handleLoginUser(w, r, account.IdAccount)
			
			if account.Gesperrt == 0 {
			
				if account.Admin == 1 {
					w.Header().Set("Content-Type", "text/html")
					w.WriteHeader(http.StatusOK)
	
					tmpl, _ := template.ParseFiles(config.PfadTemplate + "/feedback/loginFeedbackPositivAdmin.html")
	
					tmpl.ExecuteTemplate(w, "loginFeedbackPositivAdmin", nil)
				} else {
					w.Header().Set("Content-Type", "text/html")
					w.WriteHeader(http.StatusOK)
	
					tmpl, _ := template.ParseFiles(config.PfadTemplate + "/feedback/loginFeedbackPositiv.html")
	
					tmpl.ExecuteTemplate(w, "loginFeedbackPositiv", nil)
				}
				
				
			} else {
				w.Header().Set("Content-Type", "text/html")
				w.WriteHeader(http.StatusOK)
	
				tmpl, _ := template.ParseFiles(config.PfadTemplate + "/feedback/loginFeedbackNegativ.html")
	
				tmpl.ExecuteTemplate(w, "loginFeedbackNegativ", nil)
			}
		} else {

			w.Header().Set("Content-Type", "text/html")
			w.WriteHeader(http.StatusOK)

			tmpl, _ := template.ParseFiles(config.PfadTemplate + "/feedback/loginFeedbackNegativ.html")

			tmpl.ExecuteTemplate(w, "loginFeedbackNegativ", nil)

		}

	}
}
