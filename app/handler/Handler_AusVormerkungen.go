package handler

import (
	"borgdir.media/app/model"
	// "borgdir.media/config"
	"fmt"
	// "html/template"
	"net/http"
	"strconv"
)

type Handler_AusVormerkungen struct{}

func (h *Handler_AusVormerkungen) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	fmt.Println("In Warenkorb arbeitet...")

	/* Session */
	var id int
	w, r, id = handleSession(w, r)

	if id != 0 {
		
		// acc, _ := model.GetById_Account(id)
		q := r.URL.Query()
		idVormerkung, _ := strconv.Atoi( q.Get("idVormerkung") )

		vormerkung, _ := model.GetById_Vormerkung( idVormerkung )
		
		vormerkung.DeleteVormerkung()
		
		/*
		w.Header().Set("Content-Type", "text/html")
		w.WriteHeader(http.StatusOK)

		filePath := config.PfadStatic + "/index.html"
		head := config.PfadTemplate + "/head.html"
		header := config.PfadTemplate + "/headerUser.html"
		footer := config.PfadTemplate + "/footer.html"
		content := config.PfadTemplate + "/contentIndex.html"

		tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)

		tmpl.ExecuteTemplate(w, "index", acc)
		*/
		
		handlerMyEquipment := Handler_MeinEquipment{}
		handlerMyEquipment.ServeHTTP(w, r)
	} else {
	}
}
