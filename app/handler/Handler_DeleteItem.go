package handler

import (
	"borgdir.media/app/model"
	"borgdir.media/config"
	"fmt"
	"html/template"
	"net/http"
	"strconv"
)

type Handler_DeleteItem struct{}

func (h *Handler_DeleteItem) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {

		/* Session */
		var id int
		w, r, id = handleSession(w, r)

		if id != 0 {
			acc, _ := model.GetById_Account(id)

			if acc.Admin == 1 {
				fmt.Println("Account gelöscht!")

				q := r.URL.Query()

				idItem, _ := strconv.Atoi(q.Get("idItem"))
				
				item, _ := model.GetById_Item(idItem)
				
				item.DeleteItem()
				
				w.Header().Set("Content-Type", "text/html")
				w.WriteHeader(http.StatusOK)
	
				tmpl, _ := template.ParseFiles(config.PfadTemplate + "/feedback/itemGeloescht.html")
	
				tmpl.ExecuteTemplate(w, "itemGeloescht", nil)
			}

		}
	}
}
