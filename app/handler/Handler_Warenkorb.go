package handler

import (
	"borgdir.media/app/model"
	"borgdir.media/config"
	"fmt"
	"html/template"
	"net/http"
	"strconv"
	"time"
)

type Handler_Warenkorb struct{}

type WarenkorbItemSpezial struct {
	ItemSpezi		model.Item
	EquipSpezi		model.Equipment
	Rueckgabe		string
}

func (h *Handler_Warenkorb) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Warenkorb Handler serving...")

	/* Session */
	var id int
	w, r, id = handleSession(w, r)
	
	t := time.Now()

	if id != 0 {
		fmt.Println("Nutzer mit ID " + strconv.Itoa(id) + " angemeldet!")

		acc, _ := model.GetById_Account(id)

		/* Warenkorb aufbauen */
		session, _ := store.Get(r, "IdSession")
		itemsWarenkorb, _ := session.Values["Warenkorb"]

		if itemsWarenkorb != nil {
			warenkorb := []model.Item{}

			var userW = &[]model.Item{}
			userW = itemsWarenkorb.(*[]model.Item)

			fmt.Println(userW)

			for _, obj := range *userW {
				warenkorb = append(warenkorb, obj)
			}

			equipment := []WarenkorbItemSpezial{}
			for _, equip := range warenkorb {
				item, _ := model.GetById_Item(equip.IdItem)
				equip, _ := model.GetById_Equipment(equip.IdEquipment)
				
				obj := WarenkorbItemSpezial {
					ItemSpezi: item,
					EquipSpezi: equip,
					Rueckgabe: t.AddDate(0, 0, 14).Format(time.RFC3339),
				}
				equipment = append(equipment, obj)
			}

			fmt.Println(equipment)

			w.Header().Set("Content-Type", "text/html")
			w.WriteHeader(http.StatusOK)

			filePath := config.PfadHtml + "/warenkorb.html"
			head := config.PfadTemplate + "/head.html"
			header := config.PfadTemplate + "/headerUser.html"
			footer := config.PfadTemplate + "/footer.html"
			content := config.PfadTemplate + "/contentWarenkorb.html"

			tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)

			tmpl.ExecuteTemplate(w, "header", acc)
			tmpl.ExecuteTemplate(w, "warenkorb", equipment)
		} else {
			w.Header().Set("Content-Type", "text/html")
			w.WriteHeader(http.StatusOK)

			filePath := config.PfadHtml + "/warenkorb.html"
			head := config.PfadTemplate + "/head.html"
			header := config.PfadTemplate + "/headerUser.html"
			footer := config.PfadTemplate + "/footer.html"
			content := config.PfadTemplate + "/contentWarenkorb.html"

			tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)

			tmpl.ExecuteTemplate(w, "header", acc)
			tmpl.ExecuteTemplate(w, "warenkorb", nil)
		}

	} else {
		/* Warenkorb aufbauen */
		session, _ := store.Get(r, "IdSession")
		itemsWarenkorb, _ := session.Values["Warenkorb"]

		if itemsWarenkorb != nil {
			warenkorb := []model.Item{}

			var userW = &[]model.Item{}
			userW = itemsWarenkorb.(*[]model.Item)

			fmt.Println(userW)

			for _, obj := range *userW {
				warenkorb = append(warenkorb, obj)
			}

			equipment := []WarenkorbItemSpezial{}
			for _, equip := range warenkorb {
				item, _ := model.GetById_Item(equip.IdItem)
				equip, _ := model.GetById_Equipment(equip.IdEquipment)
				
				obj := WarenkorbItemSpezial {
					ItemSpezi: item,
					EquipSpezi: equip,
					Rueckgabe: t.AddDate(0, 0, 14).Format("02.01.2006"), // AddDate(0, 0, 14).Format(time.RFC3339),
				}
				equipment = append(equipment, obj)
			}

			fmt.Println(equipment)

			w.Header().Set("Content-Type", "text/html")
			w.WriteHeader(http.StatusOK)

			filePath := config.PfadHtml + "/warenkorb.html"
			head := config.PfadTemplate + "/head.html"
			header := config.PfadTemplate + "/header.html"
			footer := config.PfadTemplate + "/footer.html"
			content := config.PfadTemplate + "/contentWarenkorb.html"

			tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)

			tmpl.ExecuteTemplate(w, "header", "")
			tmpl.ExecuteTemplate(w, "warenkorb", equipment)
		} else {
			w.Header().Set("Content-Type", "text/html")
			w.WriteHeader(http.StatusOK)

			filePath := config.PfadHtml + "/warenkorb.html"
			head := config.PfadTemplate + "/head.html"
			header := config.PfadTemplate + "/header.html"
			footer := config.PfadTemplate + "/footer.html"
			content := config.PfadTemplate + "/contentWarenkorb.html"

			tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)

			tmpl.ExecuteTemplate(w, "header", "")
			tmpl.ExecuteTemplate(w, "warenkorb", nil)
		}

	}

}
