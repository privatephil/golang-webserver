package handler

import (
	"fmt"
	"borgdir.media/app/model"
	"github.com/gorilla/sessions"
	"encoding/gob"
	"net/http"
	"strconv"
)

var store = sessions.NewCookieStore([]byte("something-very-secret"))

func init() {
	fmt.Println("GOB Registrierung...")
	gob.Register(&[]model.Item{})
}

func handleSession(w http.ResponseWriter, r *http.Request) (http.ResponseWriter, *http.Request, int) {

	/* Start Session */
	session, err := store.Get(r, "IdSession")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return w, r, 0
	}

	if session.IsNew {
		fmt.Println("Eine neue Session wird angelegt!")

		session.Save(r, w)
		
		return w, r, 0
	} else if session.Values["IdAccount"] != nil {
		fmt.Println(session.Values["IdAccount"])
		
		id, _ := strconv.Atoi( session.Values["IdAccount"].(string) )
		
		acc, _ := model.GetById_Account(id)
		
		if acc.Gesperrt == 1 {
			return w, r, 0
		} else {
			return w, r, id
		}
		
		
	} else {
		fmt.Println("Eine Session existiert bereits!")
		fmt.Println( session.Values )
		
		return w, r, 0
	}

	
}

func handleLoginUser(w http.ResponseWriter, r *http.Request, IdUser int) (http.ResponseWriter, *http.Request) {

	/* Start Session */
	session, err := store.Get(r, "IdSession")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return w, r
	}

	fmt.Println("Eine neue Session mit IdUser wird angelegt!")
	/* Setting Session values */
	session.Values["IdAccount"] = strconv.Itoa(IdUser)
	session.Save(r, w)

	return w, r
}

func handleLogoutUser(w http.ResponseWriter, r *http.Request) (http.ResponseWriter, *http.Request) {

	/* Start Session */
	session, err := store.Get(r, "IdSession")
	// session, err := store.New(r, "IdSession")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return w, r
	}

	fmt.Println("Eine neue leere Session wird angelegt! Der Nutzer wurde ausgeloggt.")
	/* Setting Session values */
	session.Options.MaxAge = -1 // "Löschen" der Session
	session.Save(r, w)

	return w, r
}
