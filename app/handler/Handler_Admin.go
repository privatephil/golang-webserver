package handler

import (
	"fmt"
	"html/template"
	"net/http"
	"borgdir.media/config"
	"borgdir.media/app/model"
	"strconv"
)

type Handler_Admin struct {}

func (h *Handler_Admin) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Admin Handler serving...")
	
	/* Session */
	var id int
	w, r, id = handleSession(w, r)
	
	if id != 0 {
		fmt.Println("Nutzer mit ID " + strconv.Itoa(id) + " angemeldet!")
		
		acc, _ := model.GetById_Account(id)
			
		if acc.Admin == 1 {
			w.Header().Set("Content-Type", "text/html")
			w.WriteHeader(http.StatusOK)
			
			filePath := config.PfadHtml + "/admin.html"
			head		:= config.PfadTemplate + "/head.html"
			header 		:= config.PfadTemplate + "/headerUser.html"
			footer 		:= config.PfadTemplate + "/footer.html"
			content		:= config.PfadTemplate + "/contentAdmin.html"
			
			tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)
			
			tmpl.ExecuteTemplate(w, "admin", acc)
		} else {
			w.Header().Set("Content-Type", "text/html")
			w.WriteHeader(http.StatusOK)
			
			filePath 	:= config.PfadStatic + "/index.html"
			head		:= config.PfadTemplate + "/head.html"
			header 		:= config.PfadTemplate + "/headerUser.html"
			footer 		:= config.PfadTemplate + "/footer.html"
			content		:= config.PfadTemplate + "/contentIndex.html"
			
			tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)
			
			tmpl.ExecuteTemplate(w, "index", acc)
		}
		
	} else {
		w.Header().Set("Content-Type", "text/html")
		w.WriteHeader(http.StatusOK)
		
		filePath 	:= config.PfadStatic + "/index.html"
		head		:= config.PfadTemplate + "/head.html"
		header 		:= config.PfadTemplate + "/header.html"
		footer 		:= config.PfadTemplate + "/footer.html"
		content		:= config.PfadTemplate + "/contentIndex.html"
		
		tmpl, _ := template.ParseFiles(filePath, head, header, content, footer)
		
		tmpl.ExecuteTemplate(w, "index", "")
	}
	
}