package model

import (
	"fmt"
	"time"
	_ "github.com/mattn/go-sqlite3"
)

type Ausleihe struct {
	IdAusleihe 		int
	IdItem		 	int
	IdAccount		int
	Entliehen_am	string
	Rueckgabe_bis 	string
}

func GetAll_Ausleihe() (ausl []Ausleihe, err error) {
	rows, err := Db.Query("SELECT * FROM AUSLEIHE", nil)
	
	if err != nil {
		return
	}
	
	for rows.Next() {
		obj := Ausleihe{}
		err = rows.Scan( &obj.IdAusleihe, &obj.IdItem, &obj.IdAccount, &obj.Entliehen_am, &obj.Rueckgabe_bis )
		
		if err != nil {
			return
		}

		ausl = append(ausl, obj)
	}
	rows.Close()
	return
}

func GetById_Ausleihe(id int) (ausl Ausleihe, err error) {
	ausl = Ausleihe{}
	err = Db.QueryRow("SELECT * FROM AUSLEIHE WHERE ID_AUSLEIHE = $1", id).Scan( &ausl.IdAusleihe, &ausl.IdItem, &ausl.IdAccount, &ausl.Entliehen_am, &ausl.Rueckgabe_bis )
	return
}

func GetByUserId_Ausleihe(idUser int) (ausl []Ausleihe, err error) {
	rows, err := Db.Query("SELECT * FROM AUSLEIHE WHERE ID_ACCOUNT = $1", idUser)
	
	if err != nil {
		return
	}
	
	for rows.Next() {
		obj := Ausleihe{}
		err = rows.Scan( &obj.IdAusleihe, &obj.IdItem, &obj.IdAccount, &obj.Entliehen_am, &obj.Rueckgabe_bis )
		
		if err != nil {
			return
		}

		ausl = append(ausl, obj)
	}
	rows.Close()
	return
}

func (ausl *Ausleihe) AddAusleihe() (err error) {
	fmt.Println("Eine Ausleihe wird angelegt!")
	statement := "INSERT INTO AUSLEIHE (ID_ITEM, ID_ACCOUNT, ENTLIEHEN_AM, RUECKGABE_BIS) values ($1, $2, $3, $4)"
	stmt, err := Db.Prepare(statement)
	if err != nil {
		return
	}
	defer stmt.Close()
	_, err = stmt.Exec( ausl.IdItem, ausl.IdAccount, ausl.Entliehen_am, ausl.Rueckgabe_bis )
	return
}

func (ausl *Ausleihe) DeleteAusleihe() (err error) {
	_, err = Db.Exec( "DELETE FROM AUSLEIHE WHERE ID_AUSLEIHE = $1", ausl.IdAusleihe )
	return
}

func (ausl *Ausleihe) AusleiheVerlaengern() (err error) {
	// time.RFC3339
	date, _ := time.Parse(time.RFC3339, ausl.Rueckgabe_bis)
	
	// fmt.Println( time.Now() )
	_, err = Db.Exec( "UPDATE AUSLEIHE SET RUECKGABE_BIS = $1 WHERE ID_AUSLEIHE = $2", date.AddDate(0, 0, 14).Format(time.RFC3339), ausl.IdAusleihe )
	return
}
