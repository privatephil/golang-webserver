package model

import (
	_ "github.com/mattn/go-sqlite3"
)

type Equipment struct {
	IdEquipment 	int
	IdKategorie 	int
	Name			string
	Beschreibung	string
	Inhalt			string
	Picture			int
}

func GetAll_Equipment() (equips []Equipment, err error) {
	rows, err := Db.Query("SELECT * FROM EQUIPMENT", nil)
	
	if err != nil {
		return
	}
	
	for rows.Next() {
		obj := Equipment{}
		err = rows.Scan( &obj.IdEquipment, &obj.IdKategorie, &obj.Name, &obj.Beschreibung, &obj.Inhalt, &obj.Picture)
		
		if err != nil {
			return
		}

		equips = append(equips, obj)
	}
	rows.Close()
	return
}

func GetLastEntry_Equipment() (equip Equipment, err error) {
	equip = Equipment{}
	err = Db.QueryRow("SELECT * FROM EQUIPMENT WHERE ID_EQUIPMENT = (SELECT MAX(ID_EQUIPMENT) FROM EQUIPMENT)").Scan( &equip.IdEquipment, &equip.IdKategorie, &equip.Name, &equip.Beschreibung, &equip.Inhalt, &equip.Picture )
	return
}

func GetById_Equipment(id int) (equip Equipment, err error) {
	equip = Equipment{}
	err = Db.QueryRow("SELECT * FROM EQUIPMENT WHERE ID_EQUIPMENT = $1", id).Scan( &equip.IdEquipment, &equip.IdKategorie, &equip.Name, &equip.Beschreibung, &equip.Inhalt, &equip.Picture )
	return
}

func (equip *Equipment) AddEquipment() (err error) {
	statement := "INSERT INTO EQUIPMENT (NAME, BESCHREIBUNG, INHALT, ID_KATEGORIE, PICTURE) values ($1, $2, $3, $4, 0)"
	stmt, err := Db.Prepare(statement)
	if err != nil {
		return
	}
	defer stmt.Close()
	_, err = stmt.Exec( equip.Name, equip.Beschreibung, equip.Inhalt, equip.IdKategorie )
	return
}

func (equip *Equipment) DeleteEquipment() (err error) {
	_, err = Db.Exec( "DELETE FROM EQUIPMENT WHERE ID_EQUIPMENT = $1", equip.IdEquipment )
	return
}

func (equip *Equipment) AlterEquipment() (err error) {
	_, err = Db.Exec( "UPDATE EQUIPMENT SET NAME = $1, INHALT = $2, BESCHREIBUNG = $3, PICTURE = $4, ID_KATEGORIE = $5 WHERE ID_EQUIPMENT = $6", equip.Name, equip.Inhalt, equip.Beschreibung, equip.Picture, equip.IdKategorie, equip.IdEquipment )
	return
}