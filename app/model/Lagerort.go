package model

import (
	_ "github.com/mattn/go-sqlite3"
)

type Lagerort struct {
	IdLagerort		int
	Name		 	string
}

func GetAll_Lagerorte() (orte []Lagerort, err error) {
	rows, err := Db.Query("SELECT * FROM LAGERORTE", nil)
	
	if err != nil {
		return
	}
	
	for rows.Next() {
		obj := Lagerort{}
		err = rows.Scan( &obj.IdLagerort, &obj.Name )
		
		if err != nil {
			return
		}

		orte = append(orte, obj)
	}
	rows.Close()
	return
}

func GetById_Lagerort(id int) (ort Lagerort, err error) {
	ort = Lagerort{}
	err = Db.QueryRow("SELECT * FROM LAGERORTE WHERE ID_LAGERORT = $1", id).Scan( &ort.IdLagerort, &ort.Name )
	return
}

func (ort *Lagerort) AddLagerort() (err error) {
	statement := "INSERT INTO LAGERORTE (NAME) values ($1)"
	stmt, err := Db.Prepare(statement)
	if err != nil {
		return
	}
	defer stmt.Close()
	_, err = stmt.Exec( ort.Name )
	return
}

func (ort *Lagerort) DeleteLagerort() (err error) {
	_, err = Db.Exec( "DELETE FROM LAGERORTE WHERE ID_LAGERORT = $1", ort.IdLagerort )
	return
}