package model

import (
	_ "github.com/mattn/go-sqlite3"
)

type Kategorie struct {
	IdKategorie		int
	Name		 	string
}

func GetAll_Kategorien() (kategs []Kategorie, err error) {
	rows, err := Db.Query("SELECT * FROM KATEGORIEN", nil)
	
	if err != nil {
		return
	}
	
	for rows.Next() {
		obj := Kategorie{}
		err = rows.Scan( &obj.IdKategorie, &obj.Name )
		
		if err != nil {
			return
		}

		kategs = append(kategs, obj)
	}
	rows.Close()
	return
}

func GetById_Kategorie(id int) (kateg Kategorie, err error) {
	kateg = Kategorie{}
	err = Db.QueryRow("SELECT * FROM KATEGORIEN WHERE ID_KATEGORIE = $1", id).Scan( &kateg.IdKategorie, &kateg.Name )
	return
}

func (kateg *Kategorie) AddKategorie() (err error) {
	statement := "INSERT INTO KATEGORIEN (NAME) values ($1)"
	stmt, err := Db.Prepare(statement)
	if err != nil {
		return
	}
	defer stmt.Close()
	_, err = stmt.Exec( kateg.Name )
	return
}

func (kateg *Kategorie) DeleteKategorie() (err error) {
	_, err = Db.Exec( "DELETE FROM KATEGORIEN WHERE ID_KATEGORIE = $1", kateg.IdKategorie )
	return
}