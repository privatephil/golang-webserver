package model

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
)

var Db *sql.DB

func init() {
	var err error
	Db, err = sql.Open("sqlite3", "../src/borgdir.media/borgdir.sqlite")
	// fmt.Println("Fehler sql.Open(...): " + err.Error())
	
	if err != nil {
		panic(err)
	}
}