package model

import (
	_ "github.com/mattn/go-sqlite3"
)

type Vormerkung struct {
	IdVormerkung		int
	IdEquipment		 	int
	IdAccount			int
	Rueckgabe			string
	IdItem				int
}

func GetAll_Vormerkungen() (vorm []Vormerkung, err error) {
	rows, err := Db.Query("SELECT * FROM VORMERKUNGEN", nil)
	
	if err != nil {
		return
	}
	
	for rows.Next() {
		obj := Vormerkung{}
		err = rows.Scan( &obj.IdVormerkung, &obj.IdEquipment, &obj.IdAccount, &obj.Rueckgabe, &obj.IdItem )
		
		if err != nil {
			return
		}

		vorm = append(vorm, obj)
	}
	rows.Close()
	return
}

func GetById_Vormerkung(id int) (vorm Vormerkung, err error) {
	vorm = Vormerkung{}
	err = Db.QueryRow("SELECT * FROM VORMERKUNGEN WHERE ID_VORMERKUNG = $1", id).Scan( &vorm.IdVormerkung, &vorm.IdEquipment, &vorm.IdAccount, &vorm.Rueckgabe, &vorm.IdItem )
	return
}

func GetByUserId_Vormerkung(idUser int) (vorm []Vormerkung, err error) {
	rows, err := Db.Query("SELECT * FROM VORMERKUNGEN WHERE ID_ACCOUNT = $1", idUser)
	
	if err != nil {
		return
	}
	
	for rows.Next() {
		obj := Vormerkung{}
		err = rows.Scan( &obj.IdVormerkung, &obj.IdEquipment, &obj.IdAccount, &obj.Rueckgabe, &obj.IdItem )
		
		if err != nil {
			return
		}

		vorm = append(vorm, obj)
	}
	rows.Close()
	return
}

func (vorm *Vormerkung) AddVormerkung() (err error) {
	statement := "INSERT INTO VORMERKUNGEN (ID_EQUIPMENT, ID_ACCOUNT, RUECKGABE, ID_ITEM) values ($1, $2, $3, $4)"
	stmt, err := Db.Prepare(statement)
	if err != nil {
		return
	}
	defer stmt.Close()
	_, err = stmt.Exec( vorm.IdEquipment, vorm.IdAccount, vorm.Rueckgabe, vorm.IdItem )
	return
}

func (vorm *Vormerkung) DeleteVormerkung() (err error) {
	_, err = Db.Exec( "DELETE FROM VORMERKUNGEN WHERE ID_VORMERKUNG = $1", vorm.IdVormerkung )
	return
}