package model

import (
	_ "github.com/mattn/go-sqlite3"
	"fmt"
)

type Account struct {
	IdAccount 		int
	Benutzername 	string
	Passwort		string
	Email			string
	Admin 			int
	Gesperrt 		int
	Aktiv_bis		string
	Picture			int
}

func GetAll_Accounts() (accs []Account, err error) {
	rows, err := Db.Query("SELECT * FROM ACCOUNTS", nil)
	
	if err != nil {
		return
	}
	
	for rows.Next() {
		obj := Account{}
		err = rows.Scan( &obj.IdAccount, &obj.Benutzername, &obj.Passwort, &obj.Email, &obj.Admin, &obj.Gesperrt, &obj.Aktiv_bis, &obj.Picture )
		
		if err != nil {
			return
		}

		accs = append(accs, obj)
	}
	rows.Close()
	return
}

func GetByUsername_Account(username string) (acc Account, err error) {
	acc = Account{}
	err = Db.QueryRow("SELECT * FROM ACCOUNTS WHERE BENUTZERNAME = $1", username).Scan( &acc.IdAccount, &acc.Benutzername, &acc.Passwort, &acc.Email, &acc.Admin, &acc.Gesperrt, &acc.Aktiv_bis, &acc.Picture )
	return
}

func GetById_Account(id int) (acc Account, err error) {
	acc = Account{}
	err = Db.QueryRow("SELECT * FROM ACCOUNTS WHERE ID_ACCOUNT = $1", id).Scan( &acc.IdAccount, &acc.Benutzername, &acc.Passwort, &acc.Email, &acc.Admin, &acc.Gesperrt, &acc.Aktiv_bis, &acc.Picture )
	return
}

func Search_Accounts(arg1 string, kategorie string) (accs []Account, err error) {
	var query string = "SELECT * FROM ACCOUNTS t1"
	
	if arg1 != "" {
		query += " WHERE t1.BENUTZERNAME LIKE LOWER('%"+arg1+"%')"
		
		switch kategorie {
		case "user":
			query += " AND t1.ADMIN = 0"
		case "admin":
			query += " AND t1.ADMIN = 1"
	}
	} else {
		switch kategorie {
		case "user":
			query += " WHERE t1.ADMIN = 0"
		case "admin":
			query += " WHERE t1.ADMIN = 1"
	}
	}
	
	
	
	fmt.Println(query)
	
	rows, err := Db.Query(query, nil)
	
	if err != nil {
		return
	}
	
	for rows.Next() {
		obj := Account{}
		err = rows.Scan( &obj.IdAccount, &obj.Benutzername, &obj.Passwort, &obj.Email, &obj.Admin, &obj.Gesperrt, &obj.Aktiv_bis, &obj.Picture )
		
		if err != nil {
			return
		}

		accs = append(accs, obj)
	}
	rows.Close()
	return
}

func (acc *Account) AddAccount() (err error) {
	statement := "INSERT INTO ACCOUNTS (BENUTZERNAME, PASSWORT, EMAIL, ADMIN, GESPERRT, AKTIV_BIS, PICTURE) values ($1, $2, $3, 0, 0, $4, 0)"
	stmt, err := Db.Prepare(statement)
	if err != nil {
		return
	}
	defer stmt.Close()
	_, err = stmt.Exec( acc.Benutzername, acc.Passwort, acc.Email, acc.Aktiv_bis )
	return
}

func (acc *Account) DeleteAccount() (err error) {
	_, err = Db.Exec( "DELETE FROM ACCOUNTS WHERE ID_ACCOUNT = $1", acc.IdAccount )
	return
}

func (acc *Account) AlterAccount() (err error) {
	_, err = Db.Exec( "UPDATE ACCOUNTS SET BENUTZERNAME = $1, PASSWORT = $2, EMAIL = $3, ADMIN = $4, GESPERRT = $5, AKTIV_BIS = $6, PICTURE = $7 WHERE ID_ACCOUNT = $8", acc.Benutzername, acc.Passwort, acc.Email, acc.Admin, acc.Gesperrt, acc.Aktiv_bis, acc.Picture, acc.IdAccount )
	return
}
