package model

import (
	_ "github.com/mattn/go-sqlite3"
)

type Item struct {
	IdItem      int
	IdEquipment int
	IdLagerort  int
	Entliehen   int
}

func GetAll_Item() (items []Item, err error) {
	rows, err := Db.Query("SELECT * FROM ITEMS", nil)

	if err != nil {
		return
	}

	for rows.Next() {
		obj := Item{}
		err = rows.Scan(&obj.IdItem, &obj.IdEquipment, &obj.IdLagerort, &obj.Entliehen)

		if err != nil {
			return
		}

		items = append(items, obj)
	}
	rows.Close()
	return
}

func GetById_Item(id int) (item Item, err error) {
	item = Item{}
	err = Db.QueryRow("SELECT * FROM ITEMS WHERE ID_ITEM = $1", id).Scan(&item.IdItem, &item.IdEquipment, &item.IdLagerort, &item.Entliehen)
	return
}

func Search_Items(arg1 string, kategorie string, sortby string) (items []Item, err error) {

	var query string = "SELECT t1.ID_ITEM, t1.ID_EQUIPMENT, t1.ID_LAGERORT, t1.ENTLIEHEN FROM ITEMS t1 LEFT JOIN EQUIPMENT t2 ON t1.ID_EQUIPMENT = t2.ID_EQUIPMENT"

	if arg1 != "" {
		query += " WHERE t2.NAME LIKE LOWER('%" + arg1 + "%') OR t2.BESCHREIBUNG LIKE LOWER('%" + arg1 + "%')"

		switch kategorie {
			case "kameras":
				query += " AND t2.ID_KATEGORIE = 1"
			case "mikrofone":
				query += " AND t2.ID_KATEGORIE = 2"
			case "pa":
				query += " AND t2.ID_KATEGORIE = 3"
			case "zubehoer":
				query += " AND t2.ID_KATEGORIE = 4"
		}
	} else {
		switch kategorie {
			case "kameras":
				query += " WHERE t2.ID_KATEGORIE = 1"
			case "mikrofone":
				query += " WHERE t2.ID_KATEGORIE = 2"
			case "pa":
				query += " WHERE t2.ID_KATEGORIE = 3"
			case "zubehoer":
				query += " WHERE t2.ID_KATEGORIE = 4"
		}
	}

	if sortby == "alphabet-absteigend" {
		query += " ORDER BY t2.NAME DESC"
	} else {
		query += " ORDER BY t2.NAME ASC"
	}

	rows, err := Db.Query(query, nil)

	if err != nil {
		return
	}

	for rows.Next() {
		obj := Item{}
		err = rows.Scan(&obj.IdItem, &obj.IdEquipment, &obj.IdLagerort, &obj.Entliehen)

		if err != nil {
			return
		}

		items = append(items, obj)
	}
	rows.Close()
	return
}

func (item *Item) AddItem() (err error) {
	statement := "INSERT INTO ITEMS (ID_EQUIPMENT, ID_LAGERORT, ENTLIEHEN) values ($1, $2, $3)"
	stmt, err := Db.Prepare(statement)
	if err != nil {
		return
	}
	defer stmt.Close()
	_, err = stmt.Exec(item.IdEquipment, item.IdLagerort, 0)
	return
}

func (item *Item) DeleteItem() (err error) {
	_, err = Db.Exec("DELETE FROM ITEMS WHERE ID_ITEM = $1", item.IdItem)
	return
}

func (item *Item) AlterItem() (err error) {
	_, err = Db.Exec("UPDATE ITEMS SET ENTLIEHEN = $1, ID_LAGERORT = $2 WHERE ID_ITEM = $3", item.Entliehen, item.IdLagerort, item.IdItem)
	return
}
