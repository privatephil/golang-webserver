/* Wenn DOM rdy ist */
$(function() {
	/*readHash();

	window.onhashchange = function() {
		readHash();
	}
	*/

	$(".chosen-select").chosen();

});

function readHash() {

	switch(location.hash) {
		case "#login":
			loadPage('/html/login.html');
			break;
		case "#register":
			loadPage('/html/register.html');
			break;
		case "#equipment":
			/* loadPage('/html/equipment-content.html'); */

			$.ajax({
				method: 'GET',
				url: '/html/equipment.html',
				cache: false
			}).done(function(data) {
				$("#my-content").html(data);
				$(".chosen-select").chosen();

				appendItem("Kamera 1", ".grid-container", "/images/Canon-EOS-1300D.jpg");
				appendItem("Kamera 2", ".grid-container");
				appendItem("Stativ", ".grid-container", "/images/kamerastativ_1.jpg");
				appendItem("Mikrofon", ".grid-container");
				appendItem("Kabeltrommel", ".grid-container");
				appendItem("Monitor 7", ".grid-container");
			});
			break;
		case "#warenkorb":
			loadPage('/html/warenkorb.html');
			break;
		case "#my-equipment":
			loadPage('/html/my-equipment.html');
			break;
		case "#profil":
			loadPage('/html/profil.html');
			break;
		case "#admin":
			loadPage('/html/admin.html');
			break;
		case "#admin_equipment":
			loadPage('/html/admin/equipment.html');
			break;
		default:
			$.ajax({					/* $.ajax das selbe wie loadPage() */
				method: 'GET',
				url: '/html/index.html',
				cache: false
			}).done(function(data) {
				$("#my-content").html(data);
				$.getScript("/scripts/karussell.js");
			});
	}
}

function loadPage(url) {
	$.ajax({
		method: 'GET',
		url: url,
		cache: false
	}).done(function(data) {
		$("#my-content").html(data); /*In 'data' steht der Gesamte Inhalt der Datei, daher .html*/
		$(".chosen-select").chosen(); /*Verwendung der Dropdown-Menus von Chosen*/
	});
}

function appendItem(bezeichnung, container, picture) {

	var html = '<div class="grid-item" style="text-align: left; margin: 20px;">';

  html += '<img src='+picture+' width="150" height="135" style="float: right; margin-right: 30px; margin-left: 30px;"> <div><h3>' + bezeichnung + '</h3>';
	html += 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</div>';
	html += '<input class="btn btn-warning" type="button" value="Auswählen">';

	html += '</div>';

	$(container).append(html);
}
